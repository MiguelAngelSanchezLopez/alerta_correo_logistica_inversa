﻿using CapaDatos.Controladores;
using PruebaEnvioCorreo.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaEnvioCorreo.Controladores
{
    public class ControladorCorreoEnviado:ControladorGeneral
    {

        public void InsertarCorreoEnviado(CorreoEnviado correoEnviadoTemporal)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("InsertarCorreoEnviadoAlertaSinInformacionLogisticaInversa");
                
                EstablecerParametroProcedimientoAlmacenado(correoEnviadoTemporal.correo_al_que_se_envio, "string", "@correo_al_que_se_envio");
                EstablecerParametroProcedimientoAlmacenado(correoEnviadoTemporal.fecha_en_la_que_se_envio, "datetime", "@fecha_en_la_que_se_envio");
                
                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Boolean YaFueEnviadoUnCorreo(CorreoEnviado correoEnviadoTemporal)
        {
            try
            {
                InicializarConexion();
                
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("REVISAR_SI_YA_FUE_ENVIADO_CORREO");

                EstablecerParametroProcedimientoAlmacenado(correoEnviadoTemporal.mob_nro_ent, "int", "@mob_nro_ent");
                EstablecerParametroProcedimientoAlmacenado(correoEnviadoTemporal.correo_al_que_se_envio, "string", "@correo_al_que_se_envio");

                int conteoCorreosYaEnviados= Convert.ToInt32(EjecutarProcedimientoAlmacenadoEscalar());
                
                conexionSQL.Close();
                
                return (conteoCorreosYaEnviados > 0);
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
