﻿using CapaDatos.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Controladores
{
    public class ControladorEntregaMovilPrueba:ControladorGeneral
    {
        private List<EntregaMovilPrueba> listadoRegistrosPrueba;

        public EntregaMovilPrueba LlenarRegistroPrueba()
        {
            EntregaMovilPrueba RegistroPruebaTemporal = new EntregaMovilPrueba();
            RegistroPruebaTemporal.mob_nro_ent = Convert.ToInt32(lectorDatosSQL["mob_nro_ent"]);
            RegistroPruebaTemporal.mob_cab_nro_int = Convert.ToInt32(lectorDatosSQL["mob_cab_nro_int"]);
            RegistroPruebaTemporal.mob_cab_id_usr = Convert.ToInt32(lectorDatosSQL["mob_cab_id_usr"]);
            return RegistroPruebaTemporal;
        }

        public void LlenarListadoRegistrosPrueba()
        {
            while (lectorDatosSQL.Read())
                listadoRegistrosPrueba.Add(LlenarRegistroPrueba());
        }

        public List<EntregaMovilPrueba> ObtenerTodosRegistrosPrueba()
        {
            try
            {
                InicializarConexion();

                listadoRegistrosPrueba = new List<EntregaMovilPrueba>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("OBTENER_REGISTROS_PRUEBAS");

                LlenarLectorDatosSQL();

                LlenarListadoRegistrosPrueba();

                conexionSQL.Close();

                return listadoRegistrosPrueba;
            }
            catch (Exception ex)
            {
                return new List<EntregaMovilPrueba>();
            }

        }
        
    }
}
