﻿using CapaDatos.Controladores;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaEnvioCorreo.Controladores
{
    public class ControladorRevisionUltimaInformacionLogisticaInversa: ControladorGeneral
    {
        public string cadenaFechaUltimaCreacionObtenida = "";

        public Boolean NoHayNotificacionViajesLogisticaInversa()
        {
            String estadoEjecucionSP = "";
            Boolean respuestaProceso = new Boolean();
            respuestaProceso = false;

            try
            {
                int numeroHorasLimiteEspera= Convert.ToInt32(ConfigurationManager.AppSettings["HORAS_ESPERA"].ToString());

                InicializarConexion();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("OBTENER_FECHA_ULTIMA_CREACION_INFORMACION_LOGISTICA_INVERSA");
                
                cadenaFechaUltimaCreacionObtenida = EjecutarProcedimientoAlmacenadoEscalar().ToString();

                conexionSQL.Close();

                DateTime fechaUltimaCreacionObtenida = DateTime.Parse(cadenaFechaUltimaCreacionObtenida);
                
                DateTime fechaActual = DateTime.Now;

                //fechaActual=fechaActual.AddHours(5);

                int anioActual = fechaActual.Year;
                int mesActual = fechaActual.Month;
                int diaActual = fechaActual.Day;

                int horaActual = fechaActual.Hour;

                //int numeroHorasAdelantoServidor = 5;
                //int horaActual = fechaActual.Hour - numeroHorasAdelantoServidor;

                int minutoActual = fechaActual.Minute;
                //int segundoActual = fechaActual.Second;
                
                int anioUltimaCreacionObtenida = fechaUltimaCreacionObtenida.Year;
                int mesUltimaCreacionObtenida = fechaUltimaCreacionObtenida.Month;
                int diaUltimaCreacionObtenida = fechaUltimaCreacionObtenida.Day;
                int horaUltimaCreacionObtenida = fechaUltimaCreacionObtenida.Hour;
                int minutoUltimaCreacionObtenida = fechaUltimaCreacionObtenida.Minute;
                //int segundoUltimaCreacionObtenida = fechaUltimaCreacionObtenida.Second;

                if (anioActual > anioUltimaCreacionObtenida)
                    respuestaProceso = true;// return true;

                if (mesActual > mesUltimaCreacionObtenida)
                    respuestaProceso = true;//return true;

                if (diaActual > diaUltimaCreacionObtenida)
                    respuestaProceso = true;//return true;

                if (horaActual >= (horaUltimaCreacionObtenida+ numeroHorasLimiteEspera))
                    respuestaProceso = true;//return true;
                //if (horaActual >= (horaUltimaCreacionObtenida+2))
                //if (minutoActual >= (minutoUltimaCreacionObtenida+1))
                //    return true;

                string fechaArmadaActual = "" + diaActual + "/" + mesActual + "/" + anioActual + " " + horaActual + ":" + minutoActual;
                string fechaArmadaUltimaCreacionObtenida = "" + diaUltimaCreacionObtenida + "/" + mesUltimaCreacionObtenida + "/" + anioUltimaCreacionObtenida + " " + horaUltimaCreacionObtenida + ":" + minutoUltimaCreacionObtenida;

                string cadenasFechas = "--> fecha Actual: " + fechaArmadaActual + " - fecha ultima obtenida: " + fechaArmadaUltimaCreacionObtenida;

                if (respuestaProceso == false) {
                    estadoEjecucionSP = "Se obtuvo fecha pero no se envio el correo por que aun no se cumple el tiempo limite"+ cadenasFechas;
                    Console.WriteLine("no se envio el correo por que aun no se cumple el tiempo limite" + cadenasFechas);
                }else {
                    
                    estadoEjecucionSP = "Se obtuvo fecha correctamente"+ cadenasFechas;
                }
                //respuestaProceso = false;//return false;

            }
            catch (Exception ex)
            {
                estadoEjecucionSP = ex.Message.ToString();
                respuestaProceso = false;//return false;
            }

            try
            {
                InicializarConexion();
                conexionSQL.Open();
                EstablecerProcedimientoAlmacenado("EJECUTAR_LOG_OBTENER_FECHA_ULTIMA_CREACION_INFORMACION_LOGISTICA_INVERSA");
                EstablecerParametroProcedimientoAlmacenado(estadoEjecucionSP, "string", "@ESTADO");
                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                //estadoEjecucionSP = ex.Message.ToString();
                //respuestaProceso = false;//return false;
            }

            return respuestaProceso;
        }
    }
}
