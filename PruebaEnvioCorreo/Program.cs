﻿using CapaDatos.Controladores;
using CapaDatos.Modelos;
using CrossCutting;
using PruebaEnvioCorreo.Controladores;
using PruebaEnvioCorreo.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace PruebaEnvioCorreo
{
    class Program:ControladorGeneral
    {
        public static int SEGUNDOS_ESPERA = 0;

        public static string ObtenerAsunto()
        {
            return ConfigurationManager.AppSettings["ASUNTO_CORREO"].ToString();
        }

        public static string ObtenerMailsDestino()
        {
            return ConfigurationManager.AppSettings["MAILS_DESTINO"].ToString();
        }

        public static string ObtenerCuerpoCorreo()
        {
            
            String ArchivoCuerpoCorreo = ConfigurationManager.AppSettings["CUERPO_CORREO"].ToString();
            
            string ruta = (Path.Combine(Environment.CurrentDirectory, ArchivoCuerpoCorreo));

            string cuerpoCorreo = File.ReadAllText(ruta);
            
            return cuerpoCorreo;
        }
        
        public static string  EnviarCorreo(string destinatario, string asunto, string bodyConDatos, ControladorRevisionUltimaInformacionLogisticaInversa revisionUltimaInformacionLogisticaInversa)
        {
            Mail mail = new Mail();

            bodyConDatos=bodyConDatos.Replace("{{FECHA_EXACTA}}", revisionUltimaInformacionLogisticaInversa.cadenaFechaUltimaCreacionObtenida);
            
            return mail.SendMail(destinatario, asunto, bodyConDatos);
        }

        public static void EnviarCorreoYGuardarRegistro(string destinatario, string asunto, string body, ControladorCorreoEnviado controladorCorreoEnviado, CorreoEnviado correoEnviadoTemporal, ControladorRevisionUltimaInformacionLogisticaInversa revisionUltimaInformacionLogisticaInversa)
        {
            string respuestaObtenida = "";
            try
            {
                Console.WriteLine("enviando...");

                respuestaObtenida=EnviarCorreo(destinatario, asunto, body, revisionUltimaInformacionLogisticaInversa);

                //guardar listado de envio de correo en tabla que llevara el registro
                controladorCorreoEnviado.InsertarCorreoEnviado(correoEnviadoTemporal);
                //Console.WriteLine("el correo fue enviado");
                Console.WriteLine(respuestaObtenida);
                //respuestaObtenida = "el correo fue enviado";


            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR EN EL ENVIO: " + ex.Message );
                respuestaObtenida = "ERROR EN EL ENVIO: " + ex.Message;
            }

            try
            {
                ControladorGeneral controladorGeneral = new ControladorGeneral();
                controladorGeneral.InicializarConexion();
                controladorGeneral.conexionSQL.Open();
                controladorGeneral.EstablecerProcedimientoAlmacenado("EJECUTAR_LOG_OBTENER_FECHA_ULTIMA_CREACION_INFORMACION_LOGISTICA_INVERSA");
                controladorGeneral.EstablecerParametroProcedimientoAlmacenado(respuestaObtenida, "string", "@ESTADO");
                controladorGeneral.EjecutarProcedimientoAlmacenado();
                controladorGeneral.conexionSQL.Close();
            }
            catch (Exception ex)
            {

            }
        }

        public static void IterarDestinatarios(string[] destinatarios, string asunto, string body, ControladorCorreoEnviado controladorCorreoEnviado, ControladorRevisionUltimaInformacionLogisticaInversa revisionUltimaInformacionLogisticaInversa) {
            foreach (string destinatario in destinatarios)
            {
                CorreoEnviado correoEnviadoTemporal = new CorreoEnviado();

                correoEnviadoTemporal.mob_nro_ent = 0;
                correoEnviadoTemporal.correo_al_que_se_envio = destinatario;
                correoEnviadoTemporal.fecha_en_la_que_se_envio =  DateTime.Now;

                //Boolean yaFueEnviadoUnCorreo = controladorCorreoEnviado.YaFueEnviadoUnCorreo(correoEnviadoTemporal);

                //if (!yaFueEnviadoUnCorreo)
                EnviarCorreoYGuardarRegistro(destinatario, asunto, body, controladorCorreoEnviado, correoEnviadoTemporal, revisionUltimaInformacionLogisticaInversa);
                //else
                //    Console.WriteLine("No se envio por que ya fue enviado un correo con estos datos al destinatario");

            }
        }

        public static void IterarRegistros(string asunto, string body, string mailsDestino, ControladorRevisionUltimaInformacionLogisticaInversa revisionUltimaInformacionLogisticaInversa) {
            string[] destinatarios = mailsDestino.Split(';');

            IterarDestinatarios(destinatarios, asunto, body, new ControladorCorreoEnviado(), revisionUltimaInformacionLogisticaInversa);
        }

        public static void Main(string[] args)
        {
            //int numeroIteraciones = 0;
            //SEGUNDOS_ESPERA = Convert.ToInt32(ConfigurationManager.AppSettings["SEGUNDOS_ESPERA"].ToString());
            
            Console.WriteLine("Ejecutando consola de envios de correo sobre logistica inversa...");
            
            //var tiempoInicio = TimeSpan.Zero;
            ////obtener tiempo espera de variable de config
            //var periodoTiempoEspera = TimeSpan.FromSeconds(SEGUNDOS_ESPERA);

            //var temporizador = new System.Threading.Timer(
              //  (evento) =>
                //{
                  //  numeroIteraciones += 1;
                    //Console.WriteLine("Numero Iteraciones: " + numeroIteraciones);
                    ////VALIDAR SI HA HABIDO FALTA DE INFO DE LOGISTICA INVERSA EN LAS ULTIMAS 2 HORAS
                    ControladorRevisionUltimaInformacionLogisticaInversa revisionUltimaInformacionLogisticaInversa = new ControladorRevisionUltimaInformacionLogisticaInversa();

                    ////revisionUltimaInformacionLogisticaInversa.cadenaFechaUltimaCreacionObtenida;

                    if (revisionUltimaInformacionLogisticaInversa.NoHayNotificacionViajesLogisticaInversa())
                        IterarRegistros( ObtenerAsunto(), ObtenerCuerpoCorreo(), ObtenerMailsDestino(), revisionUltimaInformacionLogisticaInversa);

            //}, null, tiempoInicio, periodoTiempoEspera);

            Console.WriteLine("Terminando consola de envios de correo sobre logistica inversa...");

            //Console.ReadLine();
        }

    }
}
